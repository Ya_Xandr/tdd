#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "../src/RBTree.h"

using namespace CppUnit;
using namespace std;

//--- Node Test

class TestNode : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestNode);
    CPPUNIT_TEST(testInitSingleNode);
    CPPUNIT_TEST(testInitColoredNode);
    CPPUNIT_TEST(testNodeCompare);
    CPPUNIT_TEST(testAddChild);
    CPPUNIT_TEST(testHasParent);
    CPPUNIT_TEST(testHasChild);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testInitSingleNode(void);
    void testInitColoredNode(void);
    void testNodeCompare(void);
    void testAddChild(void);
    void testHasParent(void);
    void testHasChild(void);

private:
    Node<int>* node;
};
//--- Node test methods

void TestNode::setUp(void){node = new Node<int>();}
void TestNode::tearDown(void){ delete node;}

void TestNode::testInitSingleNode(){
	CPPUNIT_ASSERT(1 == node->setValue(1)->getValue());
}

void TestNode::testInitColoredNode(){
	node->setColor(RED);
	CPPUNIT_ASSERT(RED == node->getColor());
	node->setColor(BLACK);
	CPPUNIT_ASSERT(BLACK == node->getColor());
}

void TestNode::testNodeCompare(){
	node->setValue(12)->setColor(RED);
	Node<int>* testNode1 = new Node<int>();
	Node<int>* testNode2 = new Node<int>();
	testNode1->setValue(12)->setColor(BLACK);
	CPPUNIT_ASSERT(0 == (compare(node,testNode1)));
	testNode2->setValue(10)->setColor(RED);
	CPPUNIT_ASSERT(1 == (compare(node,testNode2)));
}

void TestNode::testAddChild(){
	node->setValue(10)->setColor(BLACK);
	Node<int>* testLChild = new Node<int>();
	testLChild->setValue(12)->setColor(BLACK);
	node->addChild(testLChild,LEFT);
	CPPUNIT_ASSERT(12 == node->getChild(LEFT)->getValue());
	Node<int>* testRChild = new Node<int>();
	testRChild->setValue(-1)->setColor(RED);
	node->addChild(testRChild, RIGHT);
	CPPUNIT_ASSERT(RED == node->getChild(RIGHT)->getColor());
}

void TestNode::testHasParent(){
	CPPUNIT_ASSERT(!node->hasParent());
	Node<int>* testNode = new Node<int>();
	node->addChild(testNode,LEFT);
	CPPUNIT_ASSERT(testNode->hasParent());
}

void TestNode::testHasChild(){
	CPPUNIT_ASSERT(!node->hasChild(LEFT));
	Node<int>* testNode = new Node<int>();
	node->addChild(testNode,LEFT);
	CPPUNIT_ASSERT(node->hasChild(LEFT));
	CPPUNIT_ASSERT(!node->hasChild(RIGHT));
}


//--- Tree test

class TestRBTree : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRBTree);
    CPPUNIT_TEST(testCheckAddFirstNode);
    CPPUNIT_TEST(testCheckAddNode);
    CPPUNIT_TEST(testSearchNode);
    CPPUNIT_TEST(testColoringTree);
    CPPUNIT_TEST(testRotatingTree);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testCheckAddFirstNode(void);
    void testCheckAddNode(void);
    void testSearchNode(void);
    void testColoringTree(void);
    void testRotatingTree(void);

private:
    RBTree<int> *tree;
};

//-- Tree test methods

void TestRBTree::setUp(void){tree = new RBTree<int>();}
void TestRBTree::tearDown(void){tree->~RBTree();}

void TestRBTree::testCheckAddFirstNode(){
	Node<int>* newNode = new Node<int>();
	newNode->setValue(13);
	tree->addNode(newNode);
	CPPUNIT_ASSERT(13 == tree->getRoot()->getValue());
}

void TestRBTree::testCheckAddNode(){
	Node<int>* newRoot = new Node<int>();
	newRoot->setValue(13);
	tree->addNode(newRoot);
	CPPUNIT_ASSERT(13 == tree->getRoot()->getValue());
	Node<int>* newRNode = new Node<int>();
	newRNode->setValue(14);
	tree->addNode(newRNode);
	CPPUNIT_ASSERT(14 == tree->getRoot()->getChild(RIGHT)->getValue());
	Node<int>* newLNode = new Node<int>();
	newLNode->setValue(10);
	tree->addNode(newLNode);
	CPPUNIT_ASSERT(10 == tree->getRoot()->getChild(LEFT)->getValue());
	Node<int>* secRNode = new Node<int>();
	secRNode->setValue(15);
	tree->addNode(secRNode);
	CPPUNIT_ASSERT(15 == tree->getRoot()->getChild(RIGHT)->getChild(RIGHT)->getValue());
}

void TestRBTree::testSearchNode(){
	Node<int>* newRoot = new Node<int>();
	Node<int>* child1 = new Node<int>();
	Node<int>* child2 = new Node<int>();
	newRoot->setValue(5);
	child1->setValue(6);
	child2->setValue(7);
	tree->addNode(newRoot);
	tree->addNode(child1);
	tree->addNode(child2);
	CPPUNIT_ASSERT(6 == tree->searchNode(6)->getValue());
	CPPUNIT_ASSERT(!tree->searchNode(10));
}

void TestRBTree::testColoringTree(){
	Node<int>* node5 = new Node<int>();
	node5->setValue(5);
	tree->addNode(node5);
	CPPUNIT_ASSERT(BLACK == tree->getRoot()->getColor());

	Node<int>* node6 = new Node<int>();
	node6->setValue(6);
	tree->addNode(node6);
	CPPUNIT_ASSERT(RED == tree->searchNode(6)->getColor());

	Node<int>* node7 = new Node<int>();
	node7->setValue(7);
	tree->addNode(node7);
	CPPUNIT_ASSERT(BLACK == tree->searchNode(6)->getColor()); // Expect coloring to BLACK
	CPPUNIT_ASSERT(RED == tree->searchNode(7)->getColor());

	Node<int>* node2 = new Node<int>();
	node2->setValue(2);
	tree->addNode(node2);
}

void TestRBTree::testRotatingTree(){
	Node<int>* node2 = new Node<int>();
	node2->setValue(2);
	tree->addNode(node2);
	CPPUNIT_ASSERT(BLACK == tree->getRoot()->getColor());

	Node<int>* node10 = new Node<int>();
	node10->setValue(10);
	tree->addNode(node10);
	CPPUNIT_ASSERT(RED == tree->searchNode(10)->getColor());

	Node<int>* node1 = new Node<int>();
	node1->setValue(1);
	tree->addNode(node1);

	Node<int>* node15 = new Node<int>();
	node15->setValue(15);
	tree->addNode(node15);

	Node<int>* node20 = new Node<int>();
	node20->setValue(20);
	tree->addNode(node20);

	Node<int>* node16 = new Node<int>();
	node16->setValue(16);
	tree->addNode(node16);

	CPPUNIT_ASSERT(BLACK == tree->searchNode(15)->getColor()); // Expect coloring to BLACK
	CPPUNIT_ASSERT(RED == tree->searchNode(20)->getColor());
}

//---  Test running
CPPUNIT_TEST_SUITE_REGISTRATION( TestNode );
CPPUNIT_TEST_SUITE_REGISTRATION( TestRBTree );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);

    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);

    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);

    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();

    // Output XML for Jenkins CPPunit plugin
    ofstream xmlFileOut("cppTestRBTreeResults.xml");
    XmlOutputter xmlOut(&collectedresults, xmlFileOut);
    xmlOut.write();

    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
