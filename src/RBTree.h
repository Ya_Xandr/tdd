#ifndef RBTREE_H__
#define RBTREE_H__
#include "TNode.h"

template<class T>
class RBTree {
public:
	RBTree<T>* addNode(Node<T>*);
	Node<T>* getRoot();
	RBTree<T>* setRoot(Node<T>*);
	Node<T>* searchNode(const T&);
	RBTree<T>* colorNodes(Node<T>*);
	RBTree<T>* rotateLeft(Node<T>*);
	RBTree<T>* rotateRight(Node<T>*);
private:
	Node<T>* root;
};

template<class T>
RBTree<T>* RBTree<T>::addNode(Node<T>* node) {
	if (root) {
		Node<T>* current = root;
		while (1) {
			if (compare(current, node) > 0) {
				if (current->hasChild(LEFT)) {
					current = current->getChild(LEFT);
				} else {
					current->addChild(node, LEFT);
					this->colorNodes(node);
					break;
				}
			} else {
				if (current->hasChild(RIGHT)) {
					current = current->getChild(RIGHT);
				} else {
					current->addChild(node, RIGHT);
					this->colorNodes(node);
					break;
				}
			}
		}
		return this;
	} else {
		root = node;
		this->colorNodes(node);
	}
	return this;
}

template<class T>
Node<T>* RBTree<T>::getRoot() {
	if(root){
		return root;
	}
	else{
		return false;
	}
}

template<class T>
RBTree<T>* RBTree<T>::setRoot(Node<T>* newRoot) {
	root= newRoot;
	return this;
}

template<class T>
Node<T>* RBTree<T>::searchNode(const T& value) {
	Node<T>* current = root;
	while(1){
		if(value > current->getValue() && current->hasChild(RIGHT)){
			current = current->getChild(RIGHT);
		}
		else if(value < current->getValue() && current->hasChild(LEFT)){
			current = current->getChild(LEFT);
		}
		else if(value == current->getValue()){
			return current;
		}
		else{
			return false;
		}
	}
}

template<class T>
RBTree<T>* RBTree<T>::colorNodes(Node<T>* node){
	node->setColor(RED);
	while(node != root && node->getParent()->getColor() == RED){
		if(node->getParent() == node->getParent()->getParent()->getChild(LEFT)){
			Node<T>* uncle = node->getParent()->getParent()->getChild(RIGHT);
			if(uncle && uncle->getColor() == RED){
				node->getParent()->setColor(BLACK);
				uncle->setColor(BLACK);
				node->getParent()->getParent()->setColor(RED);
				node = node->getParent()->getParent();
			} else {
				if (node == node->getParent()->getChild(RIGHT)) {
					node = node->getParent();
					rotateLeft(node);
				}
				node->getParent()->setColor(BLACK);
				node->getParent()->getParent()->setColor(RED);
				rotateRight(node->getParent()->getParent());
			}
		} else {
            Node<T>* uncle = node->getParent()->getParent()->getChild(LEFT);
            if (uncle && uncle->setColor(RED)) {
            	node->getParent()->setColor(BLACK);
            	uncle->setColor(BLACK);
                node->getParent()->getParent()->setColor(RED);
                node = node->getParent()->getParent();
            } else {
            	if (node == node->getParent()->getChild(LEFT)) {
                	node = node->getParent();
                    rotateRight(node);
                }
                node->getParent()->setColor(BLACK);
                node->getParent()->getParent()->setColor(RED);
                rotateLeft(node->getParent()->getParent());
            }
        }
	}

	root->setColor(BLACK);
}

template<class T>
RBTree<T>* RBTree<T>::rotateLeft(Node<T>* node){
	Node<T>*buf = node->getChild(RIGHT);
	Node<T>* nrChild = node->getChild(RIGHT);
	Node<T>* nParent = node->getParent();
	nrChild = buf->getChild(LEFT);
	if (buf->hasChild(LEFT)){
		Node<T>* blcParent = buf->getChild(LEFT)->getParent();
		blcParent = node;
	}

	if (buf){
		Node<T>* bParent = buf->getParent();
		bParent = node->getParent();
	}
	if (node->getParent()) {
		if (node == node->getParent()->getChild(LEFT)){
			Node<T>* nplChild = nParent->getChild(LEFT);
			nplChild = buf;
		}
		else{
			Node<T>* nprChild = nParent->getChild(RIGHT);
			nprChild = buf;
		}
	} else {
		root = buf;
	}

	Node<T>* blChild = buf->getChild(LEFT);
	blChild = node;
	if (node)
		nParent = buf;
}

template<class T>
RBTree<T>* RBTree<T>::rotateRight(Node<T>* node){
	Node<T>*buf = node->getChild(LEFT);
	Node<T>* nParent = node->getParent();
	Node<T>* nlChild = node->getChild(LEFT);
	nlChild = buf->getChild(RIGHT);
	if (buf->hasChild(RIGHT)){
		Node<T>* brcParent = buf->getChild(RIGHT)->getParent();
		brcParent = node;
	}

	if (buf){
		Node<T>* bParent = buf->getParent();
		bParent = node->getParent();
	}
	if (node->getParent()) {
		if (node == node->getParent()->getChild(RIGHT)){
			Node<T>* nprChild = nParent->getChild(RIGHT);
			nprChild = buf;
		}
		else{
			Node<T>* nplChild = nParent->getChild(LEFT);
			nplChild = buf;
		}
	} else {
		root = buf;
	}

	Node<T>* brChild = buf->getChild(RIGHT);
	brChild = node;
	if (node)
		nParent = buf;
}

#endif
