#ifndef TNODE_H__
#define TNODE_H__
#include <iostream>

typedef enum { BLACK, RED } nodeColor;
typedef enum { LEFT, RIGHT } nodeSide;

template<class T>
class Node;

template<class T>
int compare(Node<T>*, Node<T>*);

template<class T>
bool isSingleNode(Node<T>*);

template <typename T>
std::ostream& operator<<(std::ostream &, Node<T> *);

template<class T>
class Node {
	friend int compare<T>(Node<T>*, Node<T>*);
	friend bool isSingleNode<T>(Node<T>*);
	friend std::ostream& operator<< <T>(std::ostream &, Node<T> *);
public:
	Node<T>* setValue(const T&);
	Node<T>* setColor(const nodeColor&);
	nodeColor getColor(void);
	T getValue(void);
	Node<T>* getParent(void);
	Node<T>* addChild(Node<T>*, nodeSide);
	Node<T>* getChild(nodeSide);
	bool hasParent();
	bool hasChild(nodeSide);
	Node<T>* getUncle();
private:
	Node<T>* parent;
	Node<T>* lChild;
	Node<T>* rChild;
	nodeColor color;
	T nodeValue;
};

template<class T>
Node<T>* Node<T>::setValue(const T& value) {
	this->nodeValue = value;
	return this;
}

template<class T>
Node<T>* Node<T>::setColor(const nodeColor& color) {
	this->color = color;
	return this;
}

template<class T>
nodeColor Node<T>::getColor() {
	return this->color;
}

template<class T>
T Node<T>::getValue() {
	return this->nodeValue;
}

template<class T>
Node<T>* Node<T>::getParent() {
	if(this->parent){
		return this->parent;
	}
	else{
		return false;
	}
}

template<class T>
Node<T>* Node<T>::addChild(Node<T>* child, nodeSide side) {
	if (side == LEFT) {
		this->lChild = child;
		child->parent = this;
	} else if (side == RIGHT) {
		this->rChild = child;
		child->parent = this;
	} else {
		throw "Unexpected node side";
	}
	return this;
}

template<class T>
Node<T>* Node<T>::getChild(nodeSide side) {
	if (side == LEFT && lChild) {
		return lChild;
	} else if (side == RIGHT && rChild) {
		return rChild;
	} else {
		return false;
	}
}

template<class T>
bool Node<T>::hasParent() {
	if (parent) {
		return true;
	} else {
		return false;
	}
}

template<class T>
bool Node<T>::hasChild(nodeSide side) {
	if (side == RIGHT && rChild) {
		return true;
	} else if (side == LEFT && lChild) {
		return true;
	} else {
		return false;
	}
}

template<class T>
int compare(Node<T>* node1, Node<T>* node2) {
	if (node1->nodeValue > node2->nodeValue) {
		return 1;
	} else if (node1->nodeValue < node2->nodeValue) {
		return -1;
	} else {
		return 0;
	}
}

template <typename T>
std::ostream& operator<<(std::ostream &out, Node<T> *node) {
	out << "Val: " << node->nodeValue << " (";
	if(node->color == RED){
		out << "R";
	}
	else{
		out << "B";
	}
	if(node->hasParent()){
		out << " P: " << node->getParent() << " ";
	}
	out << ")";
}

#endif
